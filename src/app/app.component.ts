import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <main>
      <section class="list">
        <div class="card">
          <img src="assets/canape-jaune.png" alt="canapé jaune" />
          <div class="card-body">
            <h2 class="card-title">Canapé jaune</h2>
            <p>Livraison sous 2 semaines</p>
            <div class="card-actions">
              <button class="btn btn-primary">Commander</button>
            </div>
          </div>
        </div>
        <div class="card">
          <img src="assets/chaise-bois.png" alt="chaise bois" />
          <div class="card-body">
            <h2 class="card-title">Chaise bois</h2>
            <p>Livraison sous 3 jours</p>
            <div class="card-actions">
              <button class="btn btn-primary">Commander</button>
            </div>
          </div>
        </div>
        <div class="card">
          <img src="assets/fauteuil-jaune.png" alt="fauteuil jaune" />
          <div class="card-body">
            <h2 class="card-title">Fauteuil jaune</h2>
            <p>Rupture de stock</p>
            <div class="card-actions">
              <button class="btn btn-primary">Commander</button>
            </div>
          </div>
        </div>
      </section>
    </main>
  `,
  styles: [],
})
export class AppComponent {
  title = "tp1";
}
